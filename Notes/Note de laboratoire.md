## Note de laboratoire

### Lundi 20 mai

- Découverte des locaux
- Installations sur le PC fourni
- Discussions avec le maitre de stage
- Première recherche sur le sujet

### Mardi 21 Mai

- Installation d'un éditeur mark down

- Réunion avec le maître de stage pour préciser les objectifs du projet

- Fin de lecture du livre de référence sur le IndoorGML

- Mise en place d'un carnet de projet

  

### Mercredi 22 Mai

- Installation et utilisation de InViewer
- Mise à jour d'Unity
- Rencontre d'un des membres de l'équipe
- Recherches basiques:
  - Niveaux de vibrations disponibles
  - Sons libres de droits

### Jeudi 23 Mai

- Choix de sons sur les différentes banques de données
- Recherches sur Talkback et les spécificités à mettre en place lors de la programmation
- Article d'un utilisateur malvoyant qui commence à utiliser Android après iOS

  

### Vendredi 24 Mai

- Réunion avec équipe
- Lecture de différents articles archivés

### Lundi 27 Mai

- Premier jet du rapport d'analyse

### Mardi 28 Mai

- Chapitres 1 et 2 du rapport d'analyse terminés

### Mercredi 29 Mai

- Modélisation UML et réflexions sur le diagramme de classe

### Jeudi 30 Mai

- Fin de la modélisation UML
  - Difficultés sur le diagramme de classe, un simplifié est proposé comme solution
- Gantt à débattre avec le reste de l'équipe

### Vendredi 31 Mai

- Finalisation de la version 1 du rapport d'analyse qui est en anglais pour partage avec les membres de l'équipe. Des révisions seront apportées lors de la prochaine semaine et tout au long du stage.
- Envoi du rapport d'installation au professeur référent.
- Vu du Gantt avec l'équipe
  - Ok pour la première partie
  - Deuxième partie encore difficile à prévoir



