## Compte rendu d'installation

### Conditions d'arrivée

Mon stage se déroule dans la Pusan National University à ne pas confondre avec la Busan National University of Education. J'ai été accueillie dans le laboratoire par les chercheurs présent le Lundi 20 Mai. Leur maitrise de l'anglais n'est pas parfaite mais l'on se fait comprendre assez facilement. Il m'a été alloué un poste fixe pour la durée du stage. Celui-ci est neuf et particulièrement puissant bien que cela ne soit pas particulièrement impactant sur mon travail. Le tout est très confortable et je ne souffre d'aucun problème de ce côté ci.

J'ai rencontré mon premier coéquipier ,Arman, l'après-midi du Lundi. Celui-ci ne travaille pas encore sur le projet.

J'ai rencontré ma deuxième coéquipière le mercredi 22 Mai. La première réunion à eu lieu le 25 Mai et j'y ai rencontré mon quatrième coéquipier. Les trois ayant leur examens de fin d'année il ne seront à temps plein sur le projet qu'au environ du 20 juin. Les dates n'étant pas encore fixées, le planning peut être décalé de 3 à 4 jour pour les premières et deuxième périodes. Une mise à jour vous sera envoyée quand je posséderais plus d'informations.

Le professeur Ki-Joune LI était en voyage d'affaire toute la deuxième semaine. Nous ne pourrons discuter des avancées dans l'organisation du projet que lors de son retour.



Un changement de logement est prévu pour le 34ème jour et le labo m'a aidé à en trouver un autre.

### Le sujet

Celui ci à été clairement redéfini le deuxième jour. Il consiste en la réalisation d'une application permettant aux malvoyant de lire une carte en IndoorGML sur leur smartphone via différentes réponses de celui-ci.

Exemple : Bruit lorsque le doigt approche un mur, vibration lorsqu'il le touche.

La partie navigation n'est donc pas codée par l'équipe, celle-ci correspond en fait à l'usage qui sera fait de l'application par la personne déficiente visuelle.



Les objectifs ont été définis comme suit:

- L'exploitation de l'IndoorGML

   Le laboratoire est spécialisé dans ce format de données et le créateur de l'application Unity pour la lecture de ces données travaille dans le laboratoire. Il existe également des application pour l'édition et un lecteur web toujours développés dans le même laboratoire.

- La mise en place de l'interface utilisateur

  Celle-ci doit commencer à tâtons avec un premier jet qui devra être transmis à un professeur malvoyant de l'université. Puis une phase de test avec ce professeur et une élève malvoyante, qui aiderons à rendre l'application plus ergonomique.

- Définition de l'architecture de l'application

  Ce qui sera en réseau et ce qui sera en local sur le téléphone.

- L'implémentation

  Donc la version finale de l'application et potentiellement sa mise en service sur le Google Play Store.

### Tâches et solutions

- Faire une recherche approfondie sur le IndoorGML
  - Les documents de référence sont sur place, Mr Li à écrit le principal.
  - Diverse ressources sur internet.
  - Codes source des applications l'exploitant déjà.
- Mise en place de l'interface utilisateur
  - Création ou recherche de sons libre de droits 
  - Niveaux de vibrations par défaut d'un téléphone à définir.
  - Trigger des réactions et distances à paramétrer avec les malvoyants
- Conception de l'architecture de l'application
  - Deux options(pour le moment):
    - Plans mis par l'utilisateur seul
    - Plans téléchargés à l'entrée dans un bâtiment

- Implémentation
  - Mise en ligne
  - Tests version Google Play
  - Maintenance

### Planning

Pour le planning, les 28 premiers jours sont bloqués. D'où la très longue période d'analyse. Comme expliqué plus haut les autres membres de l'équipe de développement ont leur partiels de fin d'années. Il ne seront donc totalement libre qu'après ceux-ci. La période de programmation suivante est légèrement plus flexible; elle dure tout simplement jusqu'à ce que nos testeurs malvoyant soient disponible. Dans l'idéal nous devrions avoir un prototype disponible à ce moment. Puis nous entamerons les révisions, mise en places, tests et tout ce qui donnera un retour. Les testeurs devraient être disponibles à partir de mi juillet. N'ayant qu'une date approximative j'ai divisée la période en deux et il se trouve que cela fait 3 fois 28 jours.

![Planning](ganttdiagram.png)

Vous trouverez le planning joint au mail, si vous ne possédez pas d'éditeur Mark down.



### Conclusion

L'installation s'est très bien déroulée, le projet à été défini très vite et les tâches sont déjà programmée pour le plus gros. 

