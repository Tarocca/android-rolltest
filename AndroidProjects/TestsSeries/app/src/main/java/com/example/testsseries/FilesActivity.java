package com.example.testsseries;

import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Toast;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

public class FilesActivity extends AppCompatActivity {
    List contents = new ArrayList();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String folder = "Love"; // NAme of the directory we want to create
        File def = new File(Environment.getExternalStorageDirectory().getPath(), folder); // Define the path for the default directory of the application
        if (!def.exists()){ // Check if the specified repository is existing
            def.mkdir(); // Create the repository if he is not already existing
            Log.i("Lovable dance",def.getAbsolutePath()); // Debug Log. Mine are always nonsense
        }

        /** Here we are creating a filter that will define if a file is a GML or not.
         * Since our application aims is to open IndoorGML files we might add .txt support later. **/
        /**
        FilenameFilter GMLFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {

                if (name.endsWith(".gml")) {
                    return true;
                } else {
                    return false;
                }
            }
        };**/

        /** Here is the part that will be use by the Recycler View **/
        File maps = new File(def.getPath() + "/");
        File[] inside = maps.listFiles();
        for(int x = 0; x < inside.length; x++) {
            Toast.makeText(this, inside[x].getName(), Toast.LENGTH_SHORT).show();
            contents.add(inside[x]);
        }
        //File[] contents = def.listFiles(GMLFilter); // List that will contains every GML file in the chosen directory

        /** The adapter is what bind  recycle view to the data it shows. It needs to be bound o the Recycler view for the same reasons that the Layout Manager. **/

        if (!(contents == null)) {
            setContentView(R.layout.files_activity);
            RecyclerView rvFiles = findViewById(R.id.files_activity);
            rvFiles.setHasFixedSize(true);
            rvFiles.setLayoutManager(new LinearLayoutManager(this));
            ListAdapter adapter = new ListAdapter(contents);
            rvFiles.setAdapter(adapter);
            Log.e("Lovable Dance","Show something! Anything!");

        }
        else{
            Log.e("Lovable Dance","Contents is null");
        }

    }
        }
//}

