package com.example.testsseries;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /** Setting the permission. Since Android Marshmallow; it is required to ask for them during run time.
         * The application will check the running version of Android and asking if necessary the needed permissions  **/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){ // We check the version of android running on the device
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) // We check if the application have the right to use the internal storage
            {
                // If the version of Android is higher than Marshmallow and the application does not have the right to use internal storage we ask the use to grant it the rights to.
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},1);
            }
        }

        /** Creating the Visible elements for the activity  **/
        setContentView(R.layout.activity_main);

        /** Declaring the butoons of the main page **/
        Button files = findViewById(R.id.button_files);
        Button draw = findViewById(R.id.button_draw);
        Button selection = findViewById(R.id.button_selection);
        Button sound = findViewById(R.id.button_sound);
        Button vibration = findViewById(R.id.button_vibration);

        /** Declaration of the Listener for the Button **/
        files.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Create a new activity when the button is clicked. It permit to change the current page of the application  **/
                Intent opop = new Intent(MainActivity.this , FilesActivity.class);
                MainActivity.this.startActivity(opop);
                }
            }
        );

        sound.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.door_1);
                mp.start();
            };



    }

);
        vibration.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Vibrator vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                if (vib.hasVibrator()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        vib.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                    } else {
                        //deprecated in API 26
                        vib.vibrate(500);
                    }


                }
            }


        });
}
}